package dawson;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }

    @Test
    public void shouldReturnXProperly(){
        assertEquals("This method is to verify echo() in App class returns right value as expected,", 5, App.echo(5));

    }

    @Test   
    public void shouldReturnOneAboveX(){
        assertEquals("This method tests oneMore(), which should theoretically return x + 1", 10, App.echo(9));
    }




}
